<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tugas', function (Blueprint $table) {
            $table->id();
            $table->string('judul',50);
            $table->text('petunjuk')->nullable();
            $table->string('file');
<<<<<<< HEAD
            $table->date('tenggat');
=======
            $table->date('tenggat')->nullable();
>>>>>>> be1416bf536898234155fa5c835e809671011555
            $table->integer('kelas_id');
            $table->string('status',1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tugas');
    }
}
