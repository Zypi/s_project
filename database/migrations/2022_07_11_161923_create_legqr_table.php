<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegqrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legqr', function (Blueprint $table) {
            $table->id();
            $table->string('mahasiswa_id');
            $table->string('elegalisir_id');
            $table->string('etranskrip_id');
            $table->string('ttd');
            $table->date('tanggal_exp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legqr');
    }
}
