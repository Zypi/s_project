<?php

namespace App\Http\Controllers\E_legalisir;

use App\Http\Controllers\Controller;
use App\Models\Elegalisir;
use Illuminate\Http\Request;
use App\Repositories\E_legalisir\UserRepository;
use PDF;

class ElegalisirController extends Controller
{

    public function elegalisirA()
    {
        return view('e_legalisir.alumni.elegalisir.hal_utama_elegalisir');
    }

    public function elegalisirAlumni()
    {
        return view('e_legalisir.alumni.elegalisir.elegalisir');
    }

    public function legalisir_data()
    {
        $id = auth()->guard('mahasiswa')->user()->id;
        $legalisir = Elegalisir::all()->where('id_alumni', $id);
        return view('e_legalisir.alumni.elegalisir.history_legalisir',compact('legalisir','id'));
    }

    public function store(Request $request)
    {
        if($request->hasFile('bayar')){
            $file            = $request->bayar;
            $filenameWithExt = $file->getClientOriginalName();
            $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension       = $file->getClientOriginalExtension();
            $filenameSave    = str_replace(" ", "_", $filename).'_'.time().'.'.$extension;
            $file->move('storage_leg/bayar_L', $filenameSave);
        }

        if($request->hasFile('ijazah')){
            $file2            = $request->ijazah;
            $filenameWithExt2 = $file2->getClientOriginalName();
            $filename2        = pathinfo($filenameWithExt2, PATHINFO_FILENAME);
            $extension2       = $file2->getClientOriginalExtension();
            $filenameSave2    = str_replace(" ", "_", $filename2).'_'.time().'.'.$extension2;
            $file2->move('storage_leg/ijazah', $filenameSave2);
        }

        $item            = new Elegalisir;
        $item->id_alumni = auth()->guard('mahasiswa')->user()->id;
        $item->nim       = ucwords($request->input('nim'));
        $item->nm_mhs    = ucfirst($request->input('nm_mhs'));
        $item->file      = ucfirst($request->input('file'));
        $item->bayar     = $filenameSave;
        $item->status    = ucfirst($request->input('status'));
        $item->ijazah     = $filenameSave2;
        $item->save();
        return redirect('/stmik-legalisir/history-L')->with('success','Tugas telah dikirim.');
    }
}
