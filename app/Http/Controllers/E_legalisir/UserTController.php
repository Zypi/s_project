<?php

namespace App\Http\Controllers\E_legalisir;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Etranskrip;
use App\Repositories\E_legalisir\UserRepository;
use PDF;

class UserTController extends Controller
{
    public function index()
    {
        $transkrip = Etranskrip::all();
        return view('e_legalisir.admin.etranskrip.data_transkrip',compact('transkrip'));
    }

    public function konfirmasi($id)
    {
        $dttranskrip = Etranskrip::find($id);
        return view('e_legalisir.admin.etranskrip.konfirmasi',['Etranskrip' => $dttranskrip]);
    }

    public function tampilTTT($id)
    {
        $transkrip = Etranskrip::find($id);
        $path = base_path('public/storage_leg/transkrip/'.$transkrip->transkrip);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $pic  = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $path2 = base_path('public/storage_leg/ttd.png');
        $type2 = pathinfo($path2, PATHINFO_EXTENSION);
        $data2 = file_get_contents($path2);
        $pic2  = 'data:image/' . $type2 . ';base64,' . base64_encode($data2);
        $pdf = PDF::loadView('e_legalisir.admin.etranskrip.legalisirT', array('pic' => $pic,'pic2' => $pic2, 'transkrip' => $transkrip))
        ->setPaper('a4','potrait');
        return $pdf->download('Legalisir-Transkrip-DNBS.pdf');
        //->set_option('IsHtml5ParserEnabled',true);   
        

        //file_put_contents('legalisir-ijazah.pdf',$pdf->output());

        //chmod('legalisir-ijazah.pdf',0777);

        // return $pdf->render();
        
    }

    public function update(Request $request, $id)
    {
        $item = Etranskrip::find($id);
            $item->status = $request->status;
            if($request->hasFile('file')){
                $file = $request->file;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $filenameSave = str_replace(" ", "_", $filename).'_'.time().'.'.$extension;
                $file->move('storage_leg/file_t', $filenameSave);
                
                File::delete('storage_leg/file_t/'.$item->file);
                $item->file = $filenameSave;
            }
            $item->save();
        return redirect('/admin-legalisir/etranskrip')->with('berhasil','Data Telah Diubah');
    }   

    public function destroy($id)
    {
        $dttranskrip = Etranskrip::find($id);
        $dttranskrip -> delete();
        return redirect('/admin-legalisir/etranskrip')->with('Berhasil','Data Telah Terhapus');
    }
}
