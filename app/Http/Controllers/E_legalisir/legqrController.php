<?php

namespace App\Http\Controllers\E_legalisir;

use SimpleSoftwareIO\QrCode\Generator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\legqr;

class legqrController extends Controller
{
    public function leg()
    {
        $legqr=legqr::all();
        return view('e_legalisir.admin.data_legqr.legqr',compact('legqr'));
    }

    public function tampil()
    {
        $legqr=legqr::all();
        return view('e_legalisir.admin.data_legqr.tampil_qr',compact('legqr'));
    }

    public function data_qr()
    {
        $legqr=legqr::all();
        return view('e_legalisir.admin.data_legqr.lqrcode',compact('legqr'));
    }

    public function qrcode()
    {  
        $qrcode = new Generator;
        $data = $qrcode ->size(300)->generate('http://127.0.0.1:8000/admin-legalisir/tampil-legqr');

        return view('e_legalisir.admin.data_legqr.qrcode',compact('data'));
    }
}
