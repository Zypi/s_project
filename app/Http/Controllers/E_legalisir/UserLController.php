<?php

namespace App\Http\Controllers\E_legalisir;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Elegalisir;
use App\Repositories\E_legalisir\UserRepository;
use PDF;

class UserLController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $legalisir = Elegalisir::all();
        return view('e_legalisir.admin.elegalisir.data_legalisir',compact('legalisir'));
    }  

    public function tampilIII($id)
    {
        $legalisir = Elegalisir::find($id);
        $path = base_path('public/storage_leg/ijazah/'.$legalisir->ijazah);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $pic  = 'data:image/' . $type . ';base64,' . base64_encode($data);

        $path2 = base_path('public/storage_leg/ttd.png');
        $type2 = pathinfo($path2, PATHINFO_EXTENSION);
        $data2 = file_get_contents($path2);
        $pic2  = 'data:image/' . $type2 . ';base64,' . base64_encode($data2);
        $pdf = PDF::loadView('e_legalisir.admin.elegalisir.legalisirI', array('pic' => $pic,'pic2' => $pic2, 'legalisir' => $legalisir))
        ->setPaper('a4','landscape');
        return $pdf->download('Legalisir-Ijazah-DNBS.pdf');
        //->set_option('IsHtml5ParserEnabled',true);   
        

        //file_put_contents('legalisir-ijazah.pdf',$pdf->output());

        //chmod('legalisir-ijazah.pdf',0777);

        // return $pdf->render();
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function konfirmasi($id)
    {
        $dtlegalisir = Elegalisir::find($id);
        return view('e_legalisir.admin.elegalisir.konfirmasi',['Elegalisir' => $dtlegalisir]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Elegalisir::find($id);
            $item->status = $request->status;
            if($request->hasFile('file')){
                $file = $request->file;
                $filenameWithExt = $file->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $filenameSave = str_replace(" ", "_", $filename).'_'.time().'.'.$extension;
                $file->move('storage_leg/file_l', $filenameSave);
                
                File::delete('storage_leg/file_l/'.$item->file);
                $item->file = $filenameSave;
            }
            $item->save();
        return redirect('/admin-legalisir/elegalisir')->with('berhasil','Data Telah Diubah');
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dtlegalisir = Elegalisir::find($id);
        $dtlegalisir -> delete();
        return redirect('/admin-legalisir/elegalisir')->with('Berhasil','Data Telah Terhapus');
    }
}
