<?php

namespace App\Http\Controllers\E_legalisir;

use App\Http\Controllers\Controller;
use App\Models\Etranskrip;
use Illuminate\Http\Request;
use App\Repositories\E_legalisir\UserRepository;

class EtranskripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function etranskripA()
    {
        return view('e_legalisir.alumni.etranskrip.hal_utama_etranskrip');
    }

    public function etranskripAlumni()
    {
        return view('e_legalisir.alumni.etranskrip.etranskrip');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function transkrip_data()
    {
        $id        = auth()->guard('mahasiswa')->user()->id;
        $transkrip = Etranskrip::all()->where('id_alumni', $id);
        return view('e_legalisir.alumni.etranskrip.history_transkrip',compact('transkrip','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('bayar')){
            $file            = $request->bayar;
            $filenameWithExt = $file->getClientOriginalName();
            $filename        = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension       = $file->getClientOriginalExtension();
            $filenameSave    = str_replace(" ", "_", $filename).'_'.time().'.'.$extension;
            $file->move('storage_leg/bayar_T', $filenameSave);
        }

        if($request->hasFile('transkrip')){
            $file2            = $request->transkrip;
            $filenameWithExt2 = $file2->getClientOriginalName();
            $filename2        = pathinfo($filenameWithExt2, PATHINFO_FILENAME);
            $extension2       = $file2->getClientOriginalExtension();
            $filenameSave2    = str_replace(" ", "_", $filename2).'_'.time().'.'.$extension2;
            $file2->move('storage_leg/transkrip', $filenameSave2);
        }

        $item = new Etranskrip;
        $item->id_alumni = auth()->guard('mahasiswa')->user()->id;
        $item->nim       = ucwords($request->input('nim'));
        $item->nm_mhs    = ucfirst($request->input('nm_mhs'));
        $item->file      = ucfirst($request->input('file'));
        $item->bayar     = $filenameSave;
        $item->status    = ucfirst($request->input('status'));
        $item->transkrip = $filenameSave2;
        $item->save();
        return redirect('/stmik-legalisir/history-T')->with('success','Tugas telah dikirim.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
