<?php

namespace App\Http\Controllers\E_legalisir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Elegalisir;
use App\Models\User;
use App\Models\Mahasiswa;
use App\Models\Etranskrip;
use App\Repositories\E_legalisir\UserRepository;
use Exception;
use Auth;

class UsersController extends Controller
{

    public function fleg()
    {
        return view('e_legalisir.front.front');
    }
    
    //ALUMNI
    public function data_alumni()
    {
        $alumni = mahasiswa::all();
        return view('e_legalisir.admin.data_alumni.dt_alumni',compact('alumni'));
    }

    public function tambah_alumni()
    {
        return view('e_legalisir.admin.data_alumni.tmbh_alumni');
    }

    public function store_alumni(Request $request)
    {
        Mahasiswa::create([
            'nim'       => $request->nim,
            'nm_mhs'    => $request->nm_mhs,
            'jk'        => $request->jk,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'status'    => $request->status,
        ]);

        return redirect()->route('admin.alumni');
    }

    public function Hupdate_alumni($id)
    {
        $dtalumni = Mahasiswa::find($id);
        return view('e_legalisir.admin.data_alumni.edit_alumni',['Mahasiswa' => $dtalumni]);
    }

    public function update_alumni(Request $request, $id)
    {
        $dtalumni = Mahasiswa::find($id);
        $dtalumni -> update($request->all());
        return redirect()->route('admin.alumni')->with('berhasil','Data Telah Diedit');
    }

    public function destroy_alumni($id)
    {
        $dtalumni = Mahasiswa::find($id);
        $dtalumni -> delete();
        return redirect()->route('admin.alumni')->with('Berhasil','Data Telah Terhapus');
    }

    //ADMIN
    public function data_admin()
    {
        $user = User::all();
        return view('e_legalisir.admin.data_admin.dt_admin',compact('user'));
    }

    public function tambah_admin()
    {
        return view('e_legalisir.admin.data_admin.tmbh_admin');
    }
    
    public function store_admin(Request $request)
    {
        User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'password'  => bcrypt($request->password),
            'level'     => $request->level,
        ]);

        return redirect()->route('admin.user');
    }

    public function Hupdate_admin($id)
    {
        $dtuser = User::find($id);
        return view('e_legalisir.admin.data_admin.edit_admin',['User' => $dtuser]);
    }

    public function update_admin(Request $request, $id)
    {
        $dtuser = User::find($id);
        $dtuser -> update($request->all());
        return redirect()->route('admin.user')->with('berhasil','Data Telah Diedit');
    }

    public function destroy_admin($id)
    {
        $dtuser = User::find($id);
        $dtuser -> delete();
        return redirect()->route('admin.user')->with('Berhasil','Data Telah Terhapus');
    }

    public function loginAdmin()
    {
        return view('e_legalisir.admin.auths.login');
    }

    public function dashboardAdm()
    {
        $legalisir = Elegalisir::all();
        $transkrip = Etranskrip::all();
        return view('e_legalisir.admin.layouts.dashboard',compact('legalisir','transkrip'));
    }

    //LOGIN
    public function authentication(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:6',
        ],[
            'email.required' => 'Silahkan masukan email.',
            'email.email' => 'Email harus alamat email yang valid.',
            'email.exists' => 'Email tidak terdaftar pada sistem.',
            'password.required' => 'Silahkan masukan password.',
            'password.min' => 'Masukan password minimal :min karakter.',
        ]);
        if(Auth::guard('web')->attempt($request->only('email','password'))){
            return redirect()->route('admin.dashboard');
        }else{
            return redirect()->route('admin.login')->with('error','Login Gagal !');
        }
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->route('admin.login');
    }

    public function ubah_password()
    {
        return view('e_legalisir.admin.layouts.password');
    }

    public function password()
    {
        $dtuser = User::find($id);
        $dtuser -> update($request->all());
        return redirect('e_legalisir.admin.layouts.dashboard')->with('berhasil','Password Telah Diubah.');
    }
}
