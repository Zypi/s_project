<?php

namespace App\Http\Controllers\E_legalisir;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\E_legalisir\UserRepository;
use App\Models\Elegalisir;
use App\Models\Etranskrip;
use Exception;
use Auth;

class AlumniController extends Controller
{
    public function loginAlumni()
    {
        return view('e_legalisir.alumni.auths.login');
    }

    public function dashboardA()
    {
        $legalisir = Elegalisir::all();
        $transkrip = Etranskrip::all();
        return view('e_legalisir.alumni.layouts.dashboard',compact('legalisir','transkrip'));
    }


    public function authentication1(Request $request)
    {
        $request->validate([
            'nim' => 'required|min:9',
            'password' => 'required|min:6',
        ],[
            'nim.required' => 'Silahkan masukan nim.',
            'nim.exists' => 'NIM tidak terdaftar pada sistem.',
            'password.required' => 'Silahkan masukan password.',
            'password.min' => 'Masukan password minimal :min karakter.',
        ]);
        if(Auth::guard('mahasiswa')->attempt($request->only('nim','password'))){
            return redirect('/stmik-legalisir/dashboard');
        }else{
            return redirect('/stmik-legalisir/login')->with('error','Login Gagal !');
        }
    }

    public function logout()
    {
        Auth::guard('mahasiswa')->logout();
        return redirect('/stmik-legalisir/login');
    }
}
