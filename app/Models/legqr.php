<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Elegalisir;

class legqr extends Model
{
    use HasFactory;

    protected $table = 'legqr';

    protected $fillable =  
    [
        'elegalisir_id',
        'ttd',
        'tanggal_exp'
    ];

    public function hasleg()
    {
        return $this->hasMany(Elegalisir::class);
    }
}
