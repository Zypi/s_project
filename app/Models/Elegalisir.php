<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\legqr;

class Elegalisir extends Model
{
    use HasApiTokens, HasFactory, Notifiable;
    protected $table = "elegalisir";
    
    protected $fillable = [
        'id_alumni',
        'nim',
        'nm_mhs',
        'file',
        'bayar',
        'status',
        'ijazah'
    ];

    public function belongsTolegqr()
    {
        return $this->belongsTo(legqr::class);
    }
}
