<?php


namespace App\Repositories\E_learning;

use App\Models\Elegalisir;
use App\AppRoot\Repo\AppRepository;
use Illuminate\Http\Request;

class ElegalisirRepository extends AppRepository
{
    protected $model;
    
    public function __construct(Elegalisir $model)
    {
        $this->model = $model;
    }
    
    /**
     * set payload data for posts table.
     * 
     * @param Request $request [description]
     * @return array of data for saving.
     */
    protected function setDataPayload(Request $request)
    {
        return [
            'nim' => $request->input('nim'),
            'nm_mhs' => ucwords($request->input('nm_mhs')),
            'file' => $request->input('file'),
            'bayar' => $request->input('bayar'),
            'status' => bcrypt($request->input('status')),
        ];
    }
}