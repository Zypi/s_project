<?php

use Illuminate\Support\Facades\Route;

<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> be1416bf536898234155fa5c835e809671011555
use App\Http\Controllers\E_learning\UserController;
use App\Http\Controllers\E_learning\DosenController;
use App\Http\Controllers\E_learning\MahasiswaController;
use App\Http\Controllers\E_learning\MatkulController;
use App\Http\Controllers\E_learning\KelasController;
use App\Http\Controllers\E_learning\ForumController;
use App\Http\Controllers\E_learning\AbsensiController;
use App\Http\Controllers\E_learning\MateriController;
use App\Http\Controllers\E_learning\TugasController;
use App\Http\Controllers\E_learning\NilaiController;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->name('admin.')->group(function(){
=======
// e-legalisir
use App\Http\Controllers\E_legalisir\UsersController;
use App\Http\Controllers\E_legalisir\AlumniController;
use App\Http\Controllers\E_legalisir\ElegalisirController;
use App\Http\Controllers\E_legalisir\EtranskripController;
use App\Http\Controllers\E_legalisir\UserLController;
use App\Http\Controllers\E_legalisir\UserTController;
use App\Http\Controllers\E_legalisir\legqrController;


Route::get('/', [UsersController::class,'fleg'])->name('leg');
//ADMINISTRATOR
        //LOGIN
Route::prefix('admin-legalisir')->group(function(){
>>>>>>> 48bc4a7001ba7ff8c17417087481fd0933adf568
    Route::middleware(['guest:web'])->group(function(){
        Route::get('/login',         [UsersController::class,'loginAdmin'])->name('admin.login');
        Route::post('/auth',         [UsersController::class,'authentication'])->name('admin.auth');
    });
    Route::middleware(['auth:web'])->group(function(){
<<<<<<< HEAD
=======
<<<<<<< HEAD
>>>>>>> be1416bf536898234155fa5c835e809671011555
        Route::get('/home',[UserController::class,'home'])->name('home');
        Route::view('/password/edit','e_learning.admin.users.edit_pass')->name('password.edit');
        Route::post('/password/update',[UserController::class,'update_pass'])->name('password.update');
        Route::get('/logout',[UserController::class,'logout'])->name('logout');
        
        Route::get('/user',[UserController::class,'index'])->name('user');
        Route::view('/user/create','e_learning.admin.users.create')->name('user.create');
        Route::post('/user/store',[UserController::class,'store'])->name('user.store');
        Route::get('/user/edit/{id}',[UserController::class,'edit'])->name('user.edit');
        Route::post('/user/update/{id}',[UserController::class,'update'])->name('user.update');
        Route::get('/user/delete/{id}',[UserController::class,'delete'])->name('user.delete');

        Route::get('/mahasiswa',[MahasiswaController::class,'index'])->name('mahasiswa');
        Route::view('/mahasiswa/create','e_learning.admin.mahasiswa.create')->name('mahasiswa.create');
        Route::post('/mahasiswa/store',[MahasiswaController::class,'store'])->name('mahasiswa.store');
        Route::get('/mahasiswa/edit/{id}',[MahasiswaController::class,'edit'])->name('mahasiswa.edit');
        Route::post('/mahasiswa/update/{id}',[MahasiswaController::class,'update'])->name('mahasiswa.update');
        Route::get('/mahasiswa/delete/{id}',[MahasiswaController::class,'delete'])->name('mahasiswa.delete');
        Route::get('/mahasiswa/alumni/{id}',[MahasiswaController::class,'alumni'])->name('mahasiswa.alumni');
        Route::get('/mahasiswa/nonaktif/{id}',[MahasiswaController::class,'nonaktif'])->name('mahasiswa.nonaktif');
        Route::get('/mahasiswa/aktif/{id}',[MahasiswaController::class,'aktif'])->name('mahasiswa.aktif');

        Route::get('/dosen',[DosenController::class,'index'])->name('dosen');
        Route::view('/dosen/create','e_learning.admin.dosen.create')->name('dosen.create');
        Route::post('/dosen/store',[DosenController::class,'store'])->name('dosen.store');
        Route::get('/dosen/edit/{id}',[DosenController::class,'edit'])->name('dosen.edit');
        Route::post('/dosen/update/{id}',[DosenController::class,'update'])->name('dosen.update');
        Route::get('/dosen/delete/{id}',[DosenController::class,'delete'])->name('dosen.delete');

        Route::get('/matkul',[MatkulController::class,'index'])->name('matkul');
        Route::view('/matkul/create','e_learning.admin.matkul.create')->name('matkul.create');
        Route::post('/matkul/store',[MatkulController::class,'store'])->name('matkul.store');
        Route::get('/matkul/edit/{id}',[MatkulController::class,'edit'])->name('matkul.edit');
        Route::post('/matkul/update/{id}',[MatkulController::class,'update'])->name('matkul.update');
        Route::get('/matkul/delete/{id}',[MatkulController::class,'delete'])->name('matkul.delete');

        Route::get('/kelas',[KelasController::class,'index'])->name('kelas');
        Route::get('/kelas/create',[KelasController::class,'create'])->name('kelas.create');
        Route::post('/kelas/store',[KelasController::class,'store'])->name('kelas.store');
        Route::get('/kelas/edit/{id}',[KelasController::class,'edit'])->name('kelas.edit');
        Route::post('/kelas/update/{id}',[KelasController::class,'update'])->name('kelas.update');
        Route::get('/kelas/delete/{id}',[KelasController::class,'delete'])->name('kelas.delete');
        Route::get('/kelas/member/{id}',[KelasController::class,'member'])->name('kelas.member');
        Route::post('/kelas/member/add/{id}',[KelasController::class,'add'])->name('kelas.member.add');
        Route::get('/kelas/member/remove/{id}',[KelasController::class,'remove'])->name('kelas.member.remove');
        Route::get('/kelas/arsip/{id}',[KelasController::class,'arsip'])->name('kelas.arsip');
        Route::get('/kelas/arsip/{id}/cancel',[KelasController::class,'cancel_arsip'])->name('kelas.arsip.cancel');

        Route::get('/arsip',[KelasController::class,'arsip_kelas'])->name('arsip');
    });
});

Route::prefix('dosen')->name('dosen.')->group(function(){
    Route::middleware(['guest:dosen'])->group(function(){
        Route::view('/login','e_learning.dosen.auths.login')->name('login');
        Route::post('/postlogin',[DosenController::class,'postlogin'])->name('postlogin');
    });
    Route::middleware(['auth:dosen'])->group(function(){
        Route::get('/home',[DosenController::class,'home'])->name('home');
        Route::view('password/edit','e_learning.dosen.edit_pass')->name('password.edit');
        Route::post('password/update',[DosenController::class,'update_pass'])->name('password.update');
        Route::get('/logout',[DosenController::class,'logout'])->name('logout');
        
        Route::get('{route}/{id}/forum',[ForumController::class,'index'])->name('forum');
        Route::post('{id}/forum/store',[ForumController::class,'store'])->name('forum.store');
        Route::get('{route}/{id}/forum/{forum}/view',[ForumController::class,'view'])->name('forum.view');
        Route::post('{route}/{id}/forum/{forum}/view',[ForumController::class,'komentar'])->name('forum.komentar');

        Route::get('{route}/{id}/absensi',[AbsensiController::class,'index'])->name('absensi');
        Route::post('/{id}/absensi/store',[AbsensiController::class,'store'])->name('absensi.store');
        Route::post('/{id}/absensi/update',[AbsensiController::class,'update'])->name('absensi.update');
        
        Route::get('{route}/{id}/materi',[MateriController::class,'index'])->name('materi');
        Route::get('{route}/{id}/materi/create',[MateriController::class,'create'])->name('materi.create');
        Route::post('/{id}/materi/store',[MateriController::class,'store'])->name('materi.store');
        Route::get('{route}/{id}/materi/edit/{id2}',[MateriController::class,'edit'])->name('materi.edit');
        Route::post('/{id}/materi/update/{id2}',[MateriController::class,'update'])->name('materi.update');
        Route::get('/materi/delete/{id2}',[MateriController::class,'delete'])->name('materi.delete');

        Route::get('{route}/{id}/tugas',[TugasController::class,'index'])->name('tugas');
        Route::get('{route}/{id}/tugas/create',[TugasController::class,'create'])->name('tugas.create');
        Route::post('/{id}/tugas/store',[TugasController::class,'store'])->name('tugas.store');
        Route::get('{route}/{id}/uts/create',[TugasController::class,'create_uts'])->name('uts.create');
        Route::post('/{id}/uts/store',[TugasController::class,'store_uts'])->name('uts.store');
        Route::get('{route}/{id}/uas/create',[TugasController::class,'create_uas'])->name('uas.create');
        Route::post('/{id}/uas/store',[TugasController::class,'store_uas'])->name('uas.store');
        Route::get('{route}/{id}/tugas/edit/{id2}',[TugasController::class,'edit'])->name('tugas.edit');
        Route::post('/{id}/tugas/update/{id2}',[TugasController::class,'update'])->name('tugas.update');
        Route::get('/tugas/delete/{id2}',[TugasController::class,'delete'])->name('tugas.delete');
        Route::get('{route}/{id}/tugas/{tugas}/view',[TugasController::class,'view'])->name('tugas.view');
        Route::get('{route}/{id}/tugas/{tugas}/view/{d_tugas}',[TugasController::class,'view_tugas'])->name('tugas.view_tugas');
        Route::post('/{id}/tugas/{tugas}/view/{d_tugas}/nilai',[TugasController::class,'nilai'])->name('tugas.nilai');
<<<<<<< HEAD
        Route::get('{route}/{id}/tugas/{tugas}/view/{d_tugas}/edit',[TugasController::class,'edit_nilai'])->name('tugas.edit_nilai');
=======
>>>>>>> be1416bf536898234155fa5c835e809671011555

        Route::get('{route}/{kelas}/nilai',[NilaiController::class,'index'])->name('nilai');
        Route::get('{route}/{kelas}/nilai/create',[NilaiController::class,'create'])->name('create');
        Route::post('/{kelas}/nilai/store',[NilaiController::class,'store'])->name('store');
<<<<<<< HEAD
=======
=======
        Route::group(['middleware' => 'checkCode:Super_Admin,Approval_Admin'],function(){
            Route::get('/dashboard',     [UsersController::class,'dashboardAdm'])->name('admin.dashboard');
            Route::get('/ubah_pw',       [UsersController::class,'ubah_password'])->name('admin.ubahpassword');
            Route::get('/logout',        [UsersController::class,'logout'])->name('admin.logout');
            Route::post('/{id}/password',[UsersController::class,'password'])->name('admin.password');
            //LEGALISIR
            Route::get('/elegalisir',    [UserLController::class,'index'])->name('admin.indexL');
            //CRUD LEGALISIR
            Route::post('/{id}/update-L',[UserLController::class,'update'])->name('admin.updateL');
            Route::get('/{id}/konfir-L', [UserLController::class,'konfirmasi'])->name('admin.konfirmasiL');
            Route::get('/{id}/destroy-L',[UserLController::class,'destroy'])->name('admin.destroyL');
            Route::get('/{id}/legg',     [UserLController::class,'tampilIII'])->name('admin.tampilIII');
            //TRANSKRIP
            Route::get('/etranskrip',    [UserTController::class,'index'])->name('admin.indexT');
            //CRUD TRANSKRIP
            Route::post('/{id}/update-T',[UserTController::class,'update'])->name('admin.updateT');
            Route::get('/{id}/konfir-T', [UserTController::class,'konfirmasi'])->name('admin.konfirmasiT');
            Route::get('/{id}/destroy-T',[UserTController::class,'destroy'])->name('admin.destroyT');
            Route::get('/{id}/leggT',    [UserTController::class,'tampilTTT'])->name('admin.tampilTTT');
            //DATA ALUMNI
            Route::get('/data-alumni',   [UsersController::class,'data_alumni'])->name('admin.alumni');
            Route::get('/tmbh-alumni',   [UsersController::class,'tambah_alumni'])->name('admin.tmbhalumni');
            Route::post('/store-alumni', [UsersController::class,'store_alumni'])->name('admin.storealumni');
            Route::get('/{id}/hapus-alumni',  [UsersController::class,'destroy_alumni'])->name('admin.destroyalumni');
            Route::get('/{id}/edit-alumni',   [UsersController::class,'Hupdate_alumni'])->name('admin.hupdatealumni');
            Route::post('/{id}/update-alumni',[UsersController::class,'update_alumni'])->name('admin.updatealumni');
            //DATA ADMIN/USER
            Route::get('/data-admin',    [UsersController::class,'data_admin'])->name('admin.user');
            Route::get('/tmbh-admin',    [UsersController::class,'tambah_admin'])->name('admin.tmbhuser');
            Route::post('/store-admin',  [UsersController::class,'store_admin'])->name('admin.storeuser');
            Route::get('/{id}/hapus-admin',  [UsersController::class,'destroy_admin'])->name('admin.destroyuser');
            Route::get('/{id}/edit-admin',   [UsersController::class,'Hupdate_admin'])->name('admin.hupdateuser');
            Route::post('/{id}/update-admin',[UsersController::class,'update_admin'])->name('admin.updateuser');
            //LEG QR
            Route::get('/legqr',       [legqrController::class,'leg'])->name('admin.legqr');
            Route::get('/tampil-legqr',[legqrController::class,'tampil'])->name('admin.tampilqr');
            Route::get('/lqrcode',     [legqrController::class,'data_qr'])->name('admin.lqrcode');
            Route::get('/qrcode',      [legqrController::class,'qrcode'])->name('admin.qrcode');
        });
>>>>>>> 48bc4a7001ba7ff8c17417087481fd0933adf568
>>>>>>> be1416bf536898234155fa5c835e809671011555
    });
});
  
//ALUMNI/MAHASISWA
Route::prefix('stmik-legalisir')->group(function(){
    //LOGIN
    Route::middleware(['guest:mahasiswa'])->group(function(){
            Route::get('/login',      [AlumniController::class,'loginAlumni'])->name('alumni.login');
            Route::post('/auth',      [AlumniController::class,'authentication1'])->name('alumni.auth');
    });
    Route::middleware(['auth:mahasiswa'])->group(function(){
<<<<<<< HEAD
        Route::get('/home',[MahasiswaController::class,'home'])->name('home');
        Route::view('password/edit','e_learning.mahasiswa.edit_pass')->name('password.edit');
        Route::post('password/update',[MahasiswaController::class,'update_pass'])->name('password.update');
        Route::get('/logout',[MahasiswaController::class,'logout'])->name('logout');
<<<<<<< HEAD

        Route::get('{route}/{id}/forum',[ForumController::class,'index_mhs'])->name('forum');
        Route::post('/{id}/forum/store',[ForumController::class,'store_mhs'])->name('forum.store');
        Route::get('{route}/{id}/forum/{forum}/view',[ForumController::class,'view_mhs'])->name('forum.view');
        Route::post('{route}/{id}/forum/{forum}/view',[ForumController::class,'komentar_mhs'])->name('forum.komentar');

        Route::get('{route}/{id}/absensi',[AbsensiController::class,'index_mhs'])->name('absensi');
        Route::post('/{id}/absensi/{id2}/store',[AbsensiController::class,'store_mhs'])->name('absensi.store');

        Route::get('{route}/{id}/materi',[MateriController::class,'index_mhs'])->name('materi');

        Route::get('{route}/{id}/tugas',[TugasController::class,'index_mhs'])->name('tugas');
        Route::get('{route}/{id}/tugas/{tugas}/view',[TugasController::class,'view_mhs'])->name('tugas.view');
        Route::post('/{id}/tugas/{tugas}/store',[TugasController::class,'store_mhs'])->name('tugas.store');

        Route::get('{route}/{kelas}/nilai',[NilaiController::class,'index_mhs'])->name('nilai');
    });
});

// E-LearningDNBS
// xRptA5vxGzcPwTX#vNp2
=======

        Route::get('{route}/{id}/forum',[ForumController::class,'index_mhs'])->name('forum');
        Route::post('/{id}/forum/store',[ForumController::class,'store_mhs'])->name('forum.store');
        Route::get('{route}/{id}/forum/{forum}/view',[ForumController::class,'view_mhs'])->name('forum.view');
        Route::post('{route}/{id}/forum/{forum}/view',[ForumController::class,'komentar_mhs'])->name('forum.komentar');
>>>>>>> be1416bf536898234155fa5c835e809671011555

        Route::get('{route}/{id}/absensi',[AbsensiController::class,'index_mhs'])->name('absensi');
        Route::post('/{id}/absensi/{id2}/store',[AbsensiController::class,'store_mhs'])->name('absensi.store');

        Route::get('{route}/{id}/materi',[MateriController::class,'index_mhs'])->name('materi');

        Route::get('{route}/{id}/tugas',[TugasController::class,'index_mhs'])->name('tugas');
        Route::get('{route}/{id}/tugas/{tugas}/view',[TugasController::class,'view_mhs'])->name('tugas.view');
        Route::post('/{id}/tugas/{tugas}/store',[TugasController::class,'store_mhs'])->name('tugas.store');

        Route::get('{route}/{kelas}/nilai',[NilaiController::class,'index_mhs'])->name('nilai');
    });
});

// E-LearningDNBS
// xRptA5vxGzcPwTX#vNp2

// DBname id20048999_s_project
// DBuser id20048999_root
// DBhost localhost
// az$uy^fAx?S65gRl



=======
        Route::group(['middleware' => 'checkRole:alumni'],function(){
            Route::get('/dashboard',  [AlumniController::class,'dashboardA'])->name('alumni.dashboard');
            Route::get('/logout',     [AlumniController::class,'logout'])->name('alumni.logout');
            //LEGALISIR
            Route::get('/elegalisir', [ElegalisirController::class,'elegalisirA'])->name('alumni.elegalisir');
            Route::get('/elegalisir1',[ElegalisirController::class,'elegalisirAlumni'])->name('alumni.elegalisir1');
            Route::get('/history-L',  [ElegalisirController::class,'legalisir_data'])->name('alumni.historyL');
            //CRUD LEGALISIR
            Route::post('/elegalisirstore',[ElegalisirController::class,'store'])->name('alumni.storeL');
            //TRANSKRIP
            Route::get('/etranskrip', [EtranskripController::class,'etranskripA'])->name('alumni.etranskrip');
            Route::get('/etranskrip1',[EtranskripController::class,'etranskripAlumni'])->name('alumni.etranskrip1');
            Route::get('/history-T',  [EtranskripController::class,'transkrip_data'])->name('alumni.historyT');
            //CRUD TRANSKRIP
            Route::post('/etranskripstore',[EtranskripController::class,'store'])->name('alumni.storeT');
        });
    });
});
>>>>>>> 48bc4a7001ba7ff8c17417087481fd0933adf568
