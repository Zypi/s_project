@extends('e_legalisir.admin.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Dashboard</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">Dashboard</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<!-- Content area -->
<div class="content">
	<!-- Dashboard content -->
	<div class="row">
		<div class="col-xl-12">
			<!-- Marketing campaigns -->
			<div class="card">
				<div class="card-header header-elements-sm-inline">
					<h4 class="card-title">E-Legalisir STMIK Dharma Negara</h4>
				</div>
                <div class="card-header header-elements-sm-inline">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="font-weight-bold">Jumlah Permohonan Legalisir Ijazah</h6>
                            </div>
                            <div class="card-body">
                                <h1 class="font-weight-bold">{{ count($legalisir) }}</h1>
                            </div>
                            <a href="{{route('admin.indexL')}}" class="btn btn-primary">Buka Data Legalisir Ijazah <i class="icon-file-text3"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h6 class="font-weight-bold">Jumlah Permohonan Legalisir Transkrip</h6>
                            </div>
                            <div class="card-body">
                                <h1 class="font-weight-bold">{{ count($transkrip) }}</h1>
                            </div>
                            <a href="{{route('admin.indexT')}}" class="btn btn-primary">Buka Data Legalisir Transkrip <i class="icon-file-text2"></i></a>
                        </div>
                    </div>
				</div>
			</div>
			<!-- /marketing campaigns -->
		</div>
	</div>
	<!-- /dashboard content -->
</div>
<!-- /content area -->
@endsection