<div class="sidebar sidebar-light sidebar-main sidebar-expand-lg">

<!-- Sidebar content -->
<div class="sidebar-content">

    <!-- User menu -->
    <div class="sidebar-section">
        <div class="sidebar-user-material">
            <div class="sidebar-section-body">
                <div class="d-flex">
                    <div class="flex-1">
                    </div>
                    <div class="flex-1 text-right">
                        <button type="button" class="btn btn-outline-light border-transparent btn-icon rounded-pill btn-sm sidebar-control sidebar-main-resize d-none d-lg-inline-flex">
                            <i class="icon-transmission"></i>
                        </button>

                        <button type="button" class="btn btn-outline-light border-transparent btn-icon rounded-pill btn-sm sidebar-mobile-main-toggle d-lg-none">
                            <i class="icon-cross2"></i>
                        </button>
                    </div>
                </div>

                <div class="text-center">
                    <h6 class="mb-0 text-white text-shadow-dark mt-3">{{Auth::guard('web')->user()->name}}</h6>
                    <span class="font-size-sm text-white text-shadow-dark">{{Auth::guard('web')->user()->level}}</span>
                </div>
            </div>
                                        
            <div class="sidebar-user-material-footer">
                <a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>Pengaturan</span></a>
            </div>
        </div>

        <div class="collapse border-bottom" id="user-nav">
            <ul class="nav nav-sidebar">
                <li class="nav-item">
                    <a href="{{ route('admin.logout') }}" class="nav-link">
                        <i class="icon-switch2"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- /user menu -->


    <!-- Main navigation -->
    <div class="sidebar-section">
        <ul class="nav nav-sidebar" data-nav-type="accordion">

            <!-- Main -->
            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs mt-1">Menu</div> <i class="icon-menu" title="Main"></i></li>
            <li class="nav-item">
                <a href="{{ route('admin.dashboard') }}" class="nav-link">
                    <i class="icon-home5"></i>
                    <span>
                        Dashboard
                    </span>
                </a>
            </li>
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-user"></i> <span>User</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                    @if (auth()->user()->level=="Super_Admin")
                    <li class="nav-item"><a href="{{ route('admin.user') }}" class="nav-link"><i class="icon-user-tie"></i>Admin</a></li>
                    @endif
                    <li class="nav-item"><a href="{{ route('admin.alumni') }}" class="nav-link"><i class="icon-users"></i>Alumni</a></li>
                </ul>
            </li>
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-file-check"></i> <span>Legalisir</span></a>
                <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                    <li class="nav-item"><a href="/admin-legalisir/elegalisir" class="nav-link"><i class="icon-file-text3"></i>Ijazah</a></li>
                    <li class="nav-item"><a href="/admin-legalisir/etranskrip" class="nav-link"><i class="icon-file-text2"></i>Transkrip</a></li>
                    @if (auth()->user()->level=="SuperrrAdmin")
                    <li class="nav-item"><a href="{{ route('admin.legqr') }}" class="nav-link"><i class="icon-code"></i>QR CODE</a></li>
                    <li class="nav-item"><a href="{{ route('admin.tampilqr') }}" class="nav-link"><i class="icon-code"></i>Tampil QR CODE</a></li>
                    @endif
                </ul>
            </li>
            <!-- /main -->
    </div>
    <!-- /main navigation -->

</div>
<!-- /sidebar content -->

</div>