@extends('e_legalisir.admin.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Legalisir Ijazah</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">Legalisir Ijazah</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card col-md-12">
        <h1>Data Legalisir Ijazah</h1>
        <table class="table table-hover datatable-responsive-control-right8">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>File</th>
                    <th>Bayar</th>
                    <th>Ijazah</th>
                    <th>Status</th>
                    <th>Waktu Pesanan</th>
                    <th>aksi</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1; ?>
                @foreach($legalisir as $item)
                <tr>
                    <td class="text-center">{{$no++}}</td>
                    <td>{{ $item->nim }}</td>
                    <td>{{ $item->nm_mhs }}</td>
                    <td>
                        <p>
                            <a href="{{ asset('storage_leg/file_l/'.$item->file)}}" class="btn btn-outline-purple">Buka File</a>
                        </p>
                    </td>
                    <td>
                        <img src="{{ asset('storage_leg/bayar_L/'.$item->bayar) }}" alt="" style="width: 100px;">
                    </td>
                    <td>
                        <img src="{{ asset('storage_leg/ijazah/'.$item->ijazah) }}" alt="" style="width: 100px;">
                    </td>
                    <td>{{ $item->status }}</td>
                    <td>{{ $item->created_at }}</td>
                    <td>
                        <div class="col-lg-4">
    						<div class="text-center">
    							<div class="btn-group">
    			                	<a class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown">Proses</a>
    			                	<div class="dropdown-menu dropdown-menu-right">
                                        @if (auth()->user()->level=="Super_Admin")
                                        <a href="/admin-legalisir/{{ $item->id }}/konfir-L" class="dropdown-item"><i class="icon-file-text"></i> Kirim File</a>
                                        @endif
                                        @if (auth()->user()->level=="Approval_Admin")
    									<a href="/admin-legalisir/{{ $item->id }}/konfir-L" class="dropdown-item"><i class="icon-menu7"></i> Konfirmasi</a>
                                        @endif
    									<a href="/admin-legalisir/{{ $item->id }}/destroy-L" class="dropdown-item"><i class="icon-screen-full"></i> Hapus</a>
    								</div>
    							</div>
    	                    </div>
    					</div>
                    </td>
                    <td></td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
</div>
    @endsection