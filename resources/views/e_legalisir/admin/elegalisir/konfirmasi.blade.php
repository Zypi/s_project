@extends('e_legalisir.admin.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Legalisir Ijazah</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">Legalisir Ijazah</span>
                @if (auth()->user()->level=="Super_Admin")
                <span class="breadcrumb-item active">Kirim File</span>
                @endif
                @if (auth()->user()->level=="Approval_Admin")
                <span class="breadcrumb-item active">Konfirmasi</span>
                @endif
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <!-- Form inputs -->
    <div class="card col-md-9">
        <div class="card-header">
            <h3 class="card-title">Kirim Legalisir</h3>
            <hr>
        </div>
        <br>
        @if (auth()->user()->level=="Super_Admin")
            <a href="/admin-legalisir/{{ $Elegalisir->id }}/legg" class="btn btn-outline-indigo">Klik Untuk Legalisir Ijazah</a>
        @endif
            <div class="card-body">
            <form action="/admin-legalisir/{{$Elegalisir->id}}/update-L" method="POST" enctype="multipart/form-data">
                <fieldset class="mb-3">
                {{ csrf_field() }}
                @if (auth()->user()->level=="Approval_Admin")
                <div class="form-group row">
                        <label class="col-lg-3 col-form-label pt-0">Konfirmasi Legalisir</label>
                        <div class="col-lg-9">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="status" value="Konfirmasi" @if ($Elegalisir->status == 'Konfirmasi') checked @endif >
                                    Konfirmasi
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="status" value="Ditolak" @if ($Elegalisir->status == 'Ditolak') checked @endif >
                                    Tolak
                                </label>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if (auth()->user()->level=="Super_Admin")
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label pt-0">Status Legalisir</label>
                        <div class="col-lg-9">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="status" value="Selesai" >
                                    Selesai
                                </label>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if (auth()->user()->level=="Super_Admin")
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">file</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control-plaintext" name="file">
                        </div>
                    </div>
                    @endif
                </fieldset>
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Konfirmasi</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->
</div>
@endsection