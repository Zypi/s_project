@extends('e_legalisir.admin.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Data Admin</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">User</span>
                <span class="breadcrumb-item active">Data Admin</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card col-md-12">
        <h1>Data Admin</h1>
        <div class="md-4" align="right">
            <a href="{{ route('admin.tmbhuser') }}" class="btn btn-outline-teal">Tambah Admin <i class="icon-plus"></i></a>
        </div>
        <table class="table table-hover datatable-responsive-control-right5">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Level</th>
                    <th>Aksi</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1; ?>
                @foreach($user as $item)
                <tr>
                    <td class="text-center">{{$no++}}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->level }}</td>
                    <td>
                        <div class="col-lg-4">
    						<div class="text-center">
    							<div class="btn-group">
    			                	<a class="btn btn-outline-indigo dropdown-toggle" data-toggle="dropdown">Setting</a>
    			                	<div class="dropdown-menu dropdown-menu-right">
    									<a href="{{ route ('admin.hupdateuser',$item->id) }}" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>
    									<a href="{{ route ('admin.destroyuser',$item->id) }}" class="dropdown-item"><i class="icon-trash"></i> Hapus</a>
    								</div>
    							</div>
    	                    </div>
    					</div>
                    </td>
                    <td></td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
</div>
    @endsection