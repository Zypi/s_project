@extends('e_legalisir.admin.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Tambah Admin</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">User</span>
                <span class="breadcrumb-item active">Data Admin</span>
                <span class="breadcrumb-item active">Tambah Admin</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <!-- Form inputs -->
    <div class="card col-md-9">
        <div class="card-header">
            <h3 class="card-title">Isi Data Admin</h3>
            <hr>

        </div>
        <br>
        <div class="card-body">

            <form action="{{ route('admin.storeuser') }}" method="POST" enctype="multipart/form-data">
                <fieldset class="mb-3">
                    @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Nama</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="name" autofocus autocomplete="off" value="" required> 
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Email</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="email" autocomplete="off" value="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Password</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="password" autocomplete="off" value="" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label pt-0">Level</label>
                        <div class="col-lg-9">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="level" value="Super_Admin">
                                    Super Admin
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input" name="level" value="Approval_Admin">
                                    Approval Admin
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->

</div>
          @endsection