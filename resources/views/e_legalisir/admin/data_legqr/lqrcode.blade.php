@extends('e_legalisir.admin.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Daftar QR CODE</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">QR CODE</span>
                <span class="breadcrumb-item active">Daftar QR CODE</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card col-md-12">
        <h1>Daftar QR CODE</h1>
        <table class="table table-hover datatable-responsive-control-right4">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th>ID</th>
                    <th>Tanda Tangan</th>
                    <th>Tanggal Kadaluarsa</th>
                    <th>aksi</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1; ?>
                @foreach($legqr as $item)
                <tr>
                    <td class="text-center">{{$no++}}</td>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->ttd }}</td>
                    <td>{{ $item->tanggal_exp }}</td>
                    <td>
                        <div class="col-lg-4">
    						<div class="text-center">
    							<div class="btn-group">
    			                	<a class="btn btn-outline-danger">Hapus</a>
    								</div>
    							</div>
    	                    </div>
    					</div>
                    </td>
                    <td></td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
</div>
    @endsection