@extends('e_legalisir.admin.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>QR CODE</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
        <a href="{{ route('admin.lqrcode') }}" class="btn btn-outline-warning">Daftar QR CODE <i class="icon-plus"></i></a>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">QR CODE</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card col-md-12">
        <h1>QR CODE</h1>
        <table class="table table-hover datatable-responsive-control-right7">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th>Nama</th>
                    <th>NIM</th>
                    <th>Tanggal Permohonan</th>
                    <th>Tanggal Kadaluarsa</th>
                    <th>Tanda Tangan</th>
                    <th>aksi</th>       
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1; ?>
                @foreach($legqr as $item)
                <tr>
                    <td class="text-center">{{$no++}}</td>
                    <td>
                        @foreach($item->hasleg as $b)
                            {{$b->nm_mhs}}
                    </td>
                    <td>
                            {{$b->nim}}
                    </td>
                    <td>
                            {{$b->created_at}}
                        @endforeach
                    </td>
                    <td>{{ $item->tanggal_exp }}</td>
                    <td>{{ $item->ttd }}</td>
                    <td>
                        <div class="col-lg-4">
    						<div class="text-center">
    							<div class="btn-group">
    			                	<a class="btn btn-outline-primary dropdown-toggle" data-toggle="dropdown">Proses</a>
    			                	<div class="dropdown-menu dropdown-menu-right">
                                        <a href="{{ route('admin.qrcode') }}" class="dropdown-item"><i class="icon-file-text"></i> Buat Kode QR</a>
    									<a href="/admin-legalisir/{{ $item->id }}/destroy-L" class="dropdown-item"><i class="icon-screen-full"></i> Hapus</a>
    								</div>
    							</div>
    	                    </div>
    					</div>
                    </td>
                    <td></td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
</div>
    @endsection