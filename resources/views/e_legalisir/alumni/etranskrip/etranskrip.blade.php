@extends('e_legalisir.alumni.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Kirim Permohonan Legalisir Transkrip</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">Legalisir Transkrip</span>
                <span class="breadcrumb-item active">Permohonan Legalisir Transkrip</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <!-- Form inputs -->
    <div class="card col-md-9">
        <div class="card-header">
            <h3 class="card-title">Isi Data Permohonan Legalisir Transkrip</h3>
            <hr>

        </div>
        <br>
        <div class="card-body">

            <form action="/stmik-legalisir/etranskripstore" method="POST" enctype="multipart/form-data">
                <fieldset class="mb-3">
                    @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">NIM</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="nim" autofocus autocomplete="off" value="{{Auth::guard('mahasiswa')->user()->nim}}" readonly>
                            @error('nidn')
                                <label id="with_icon-error" class="validation-invalid-label" for="with_icon">{{$message}}</label>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Nama Lengkap</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="nm_mhs" autocomplete="off" value="{{Auth::guard('mahasiswa')->user()->nm_mhs}}" readonly>
                                <label id="with_icon-error" class="validation-invalid-label" for="with_icon"></label>
                        </div>
                    </div>
                    <input type="hidden" class="form-control-plaintext" name="file" id="file" autocomplete="off" value="">
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Bayar</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control-plaintext" name="bayar" id="bayar" autocomplete="off" value="" required>
                            @error('bayar')
                            <label id="with_icon-error" class="validation-invalid-label" for="with_icon"></label>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3">Gambar Transkrip</label>
                        <div class="col-lg-9">
                            <input type="file" class="form-control-plaintext" name="transkrip" id="transkrip" autocomplete="off" value="" required>
                            @error('bayar')
                            <label id="with_icon-error" class="validation-invalid-label" for="with_icon"></label>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-9">
                            <input type="hidden" class="form-control" name="status" id="status" autocomplete="off" value="diproses" readonly>
                            <label id="with_icon-error" class="validation-invalid-label" for="with_icon"></label>
                        </div>
                    </div>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Kirim</button>
                </div>
            </form>
        </div>
    </div>
    <!-- /form inputs -->

</div>
          @endsection