@extends('e_legalisir.alumni.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>History Permohonan Legalisir Transkrip</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">Legalisir Transkrip</span>
                <span class="breadcrumb-item active">History Legalisir Transkrip</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
<div class="card col-md-12">
<h1>History Permohonan Legalisir Transkrip</h1>
<hr>
        <table class="table table-hover datatable-responsive-control-right5">
            <thead>
                <tr>
                    <th class="text-center">No</th>
                    <th>Nama</th>
                    <th>File</th>
                    <th>Bayar</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1; ?>
                @foreach($transkrip as $item)
            <tr>
            <td class="text-center">{{$no++}}</td>
            <td>{{ $item->nm_mhs}}</td>
                    <td>
                        <p>
                            <a href="{{ asset('storage_leg/file_t/'.$item->file)}}" class="btn btn-outline-purple">Download File</a>
                        </p>
                    </td>
                    <td>
                        <img src="{{ asset('storage_leg/bayar_T/'.$item->bayar) }}" alt="" style="width: 100px;">
                    </td>
                    <td>{{ $item->status }}</td>
                    <td></td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
    </div>
    @endsection