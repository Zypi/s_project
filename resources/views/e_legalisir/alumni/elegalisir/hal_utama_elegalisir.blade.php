@extends('e_legalisir.alumni.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Legalisir Ijazah</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">Legalisir Ijazah</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card col-md-12">
        <h1>Legalisir Ijazah STMIK Dharma Negara</h1>
        <h4>Info Pembayaran:</h4>
        <h5>No rek 0000-0000-00 / BCA</h5>
        <h5>Untuk bayar sertakan bukti transfer.</h5>
            <div class="form-group">
                <a href="/stmik-legalisir/elegalisir1" class="btn btn-outline-primary">Pesan Legalisir Ijazah<i class="icon-enter2 ml-2"></i></a>
                <a href="/stmik-legalisir/history-L" class="btn btn-outline-success">History Pesan Legalisir Ijazah<i class="icon-enter2 ml-2" align="right"></i></a>
            </div>
    <!-- /control position -->

</div>
    </div>
</div>
@endsection