<div class="sidebar sidebar-light sidebar-main sidebar-expand-lg">

<!-- Sidebar content -->
<div class="sidebar-content">

    <!-- User menu -->
    <div class="sidebar-section">
        <div class="sidebar-user-material">
            <div class="sidebar-section-body">
                <div class="d-flex">
                    <div class="flex-1">
                    </div>
                    <div class="flex-1 text-right">
                        <button type="button" class="btn btn-outline-light border-transparent btn-icon rounded-pill btn-sm sidebar-control sidebar-main-resize d-none d-lg-inline-flex">
                            <i class="icon-transmission"></i>
                        </button>

                        <button type="button" class="btn btn-outline-light border-transparent btn-icon rounded-pill btn-sm sidebar-mobile-main-toggle d-lg-none">
                            <i class="icon-cross2"></i>
                        </button>
                    </div>
                </div>

                <div class="text-center">
                    <h6 class="mb-0 text-white text-shadow-dark mt-3">{{Auth::guard('mahasiswa')->user()->nm_mhs}}</h6>
                    <span class="font-size-sm text-white text-shadow-dark">{{Auth::guard('mahasiswa')->user()->nim}}</span>
                </div>
            </div>
                                        
            <div class="sidebar-user-material-footer">
                <a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>Pengaturan</span></a>
            </div>
        </div>

        <div class="collapse border-bottom" id="user-nav">
            <ul class="nav nav-sidebar">
                <li class="nav-item">
                    <a href="{{ route('alumni.logout') }}" class="nav-link">
                        <i class="icon-switch2"></i>
                        <span>Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- /user menu -->


    <!-- Main navigation -->
    <div class="sidebar-section">
        <ul class="nav nav-sidebar" data-nav-type="accordion">

            <!-- Main -->
            <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs mt-1">Menu</div> <i class="icon-menu" title="Main"></i></li>
            <li class="nav-item">
                <a href="{{ route('alumni.dashboard') }}" class="nav-link">
                    <i class="icon-home4"></i>
                    <span>
                        Dashboard
                    </span>
                </a>
            </li>
            <li class="nav-item nav-item-submenu">
                <a href="#" class="nav-link"><i class="icon-stack"></i> <span>Legalisir</span></a>

                <ul class="nav nav-group-sub" data-submenu-title="Starter kit">
                    <li class="nav-item"><a href="/stmik-legalisir/elegalisir" class="nav-link">Ijazah</a></li>
                    <li class="nav-item"><a href="/stmik-legalisir/etranskrip" class="nav-link">Transkrip</a></li>
                </ul>
            </li>
            <!-- /main -->
    </div>
    <!-- /main navigation -->

</div>
<!-- /sidebar content -->

</div>