@extends('e_legalisir.alumni.layouts.master')
@section('content')
<!-- Page header -->
<div class="page-header page-header-light">
	<div class="page-header-content header-elements-lg-inline">
		<div class="page-title d-flex">
			<h4>Dashboard</h4>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
	<div class="breadcrumb-line breadcrumb-line-light header-elements-lg-inline">
		<div class="d-flex">
			<div class="breadcrumb">
				<a class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
				<span class="breadcrumb-item active">Dashboard</span>
			</div>
			<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
		</div>
	</div>
</div>
<!-- /page header -->
<div class="content">
    <div class="card col-md-12">
        <h1>E-Legalisir STMIK Dharma Negara</h1>
    <div class="content">
    <!-- Control position -->
        <hr>
            <h4>Syarat dan ketentuan</h4>
            <h5>1. Harga transkrip ijazah Rp. 10.000</h5>
			<h5>2. Upload scan ijazah hitam putih dengan format .jpg</h5>
            <h5>3. Pastikan scan ijazah dengan posisi yang benar.</h5>
            <h5>4. Pembayaran dilakukan melalui transfer bank.</h5>
			<h5>5. File yang sudah diproses bisa didownload dihalaman history.</h5>
    <!-- /control position -->

        </div>
    </div>
</div>
@endsection